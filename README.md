# README #

This is Print & Mail's *seed-style* store. It uses the [Polymer 1.x](https://www.polymer-project.org/1.0/) framework on top of HTML, JavaScript, and CSS. It can be forked and modified to become the storefront for another store which sells static products.

#### TODO (may never happen) ####

* Enable the use of variable data products
* Link to T-Shirts

#### What is a *seed-style* store? ####

The idea of a seed store is that the entire structure of the store is a tree (like the data structure) based on a single root node. With a pointer to this root, the entire store can be built. Look at *Setting up the Product Tree* under *How do I make a new Store?*



## How do I make a new store? ##

One of the great advantages of this app is that it can be copied and "easily" modified to become a new storefront. Here is a brief outline followed by detailed instructions:

* Set up a product tree in the database
* Copy all files and rename the root folders (front- and back-end)
* Delete old images and move all needed images into the images folder
* Point ajax calls to back-end
* Change the RECSOURCE in the back-end
* Change the checkout interface parameters
* Update the splash page
* Update the css
* Create css for the checkout (optional)

### Setting up the Product Tree ###

Live and Dev Database

At Print and Mail, we have two databases (live and dev), so the following instructions must be performed in both databases. Make sure that all information put in one database is identical to the information in the other. If, for example, a primary key is used in one database by another entry but not the other database, do not use that primary key at all in your store. All of this will help you test your new store and will greatly facilitate moving from development to production.

Overview

A product tree is made up of three main components: the root, branch nodes, and leaf nodes. The root is the seed for the tree. Branch nodes group similar products together. Leaf nodes are the actual products that you want to sell. Each of these components is a row in the IPRODUCT table.

Making the root

To make the root or root product, create a new row in the IPRODUCT table. You can use the root for Seed Store as a template (PRODUCTID 2000). The important fields to fill out are PRODUCTID, PRODUCTOWNER, PRODUCTVENDOR, PRODUCTCASE, PRODUCTTYPE, and DISPLAYNAME. PRODUCTID is the primary key. PRODUCTCASE must be 'GROUP' and PRODUCTTYPE must be 'SUBGROUP'. PRODUCTOWNER and PRODUCTVENDOR must match the RECSOURCE (explained below). There are other fields that can be filled out, but they will not be used. There are other fields that must have values, so put in values to make Oracle happy. Note that the root product is not a sellable product.

The RECSOURCE

You must also make a RECSOURCE. RECSOURCE is short for record source and it identifies the store. It will also identify which products belong to the store (PRODUCTOWNER in the IPRODUCT table). For Seed Store, the RECSOURCE is SIMATERIAL. Go figure. The RECSOURCE is defined in the VRECSOURCEEXTRA table and must be unique, descriptive of the store, and one word in all capitals. The other important field in the VRECSOURCE table is the PRODUCTROOT. This is the PRODUCTID of the root product, as defined above. This is critical! Other fields may need default values, but they will not be used in the store.

Making a Leaf Product

Making a product, or a leaf product, sometimes can be a detailed process, so we only give a simple overview here. Essentially, a product has a single row in the IPRODUCT table and 0 or more rows in the IPRODUCTDTL table. Between these two tables, the product is defined and the details of how it is made are described.

Making a Branch Product

A branch node or branch product is similar to making the root product. The only difference is that you do not make a separate RECSOURCE for branch products. Like root products, they are not sellable.

Building The Tree

Now that the root is set up and you have some branch and leaf products, you can start filling out the tree. Following the tree paradigm, the root product needs to point to child products. The root and branch products can point to branch and leaf products. Nothing points to the root (except the RECSOURCE) and leaf products don't point to anything. The IPRODUCTDTL table enables us to build this one-to-many relationship. Create a row in the IPRODUCTDTL table. The PRODUCTID is the parent product and the INFO is the PRODUCTID of the child. Set the ITEM field to 'ANOTHER PRODUCT'. DISPLAYORDER can be set to determine the order the products will be displayed on the website. (null) comes after any specified numbers. You should now be able to build a full product tree.

Other Types of Branch Nodes

In Seed Store, there are some products that come in multiple languages. Instead of displaying all of the almost-identical-but-only-differ-by-language products on the same page, a Language Group groups these items together and displays a simple dropdown menu on the website. Simply create a branch node, but instead of 'SUBGROUP' for the PRODUCTTYPE, use 'LANGUAGEGROUP'. Then, have its children be the leaf products which differ only by language. Be sure that these leaf products have a specified language in the IPRODUCT table in the LANGUAGE field.

Interpreting the Product Tree

You now have a product tree. Great! But what does it mean? The product tree is the structure of the store. The children of the root product will be displayed on the Splash page. See *Updating the Splash Page*. These products essentially styled links that, when clicked, cause the website to display its children. If a displayed product is a leaf product, it will take the user to a purchasing page instead of showing more products. Check out [Seed Store Store](https://printandmail.byu.edu/seedstore/) for an example.

### Copying Files ###

To make a new store based off of Seed Store, you will need all the same code, but with some changes to make it your own. Simply download this repository (and the backend code), delete the .git folder, and rename the home directory to whatever you would like. It is nice if this name can be the same as (or at least similar to) the RECSOURCE. The backend code should be named <newname>Backend. Create a new bitbucket repository to start following and saving your changes. Since this store has a lot of images, this process may take a few minutes, sorry!

### Images ###

To show the customer what s/he is buying, you need some pictures of the products. This store follows a simple convention. We will use Seed Store as the example.

#### Product Images ####
Product images are store in seedstore/images/products. Within this folder, each product gets its own folder with the PRODUCTID as the folder name (i.e. seedstore/images/products/123456). The code looks for three types of images in that folder: ```[main|thumbnail|preview*].*```. These are literal names where ```.*``` means any image type (.jpg, .png, etc.) and ```preview*.*``` allows for "preview01.png", "preview02.png", etc.

The intent of the thumbnail is to show a small image of the product as the user is browsing products.

The main image is intended to show a larger image when the user is purchasing the product.

The previews are useful for products which need more than the main image to show its full contents (for example a training booklet with many different pages). The preview images should be the same dimensions as the main image for the best user experience. They will be shown in the same place as the main image with arrow buttons to navigate between them. They are ordered lexicographically.

You will need to make a new folder for each product and move images into these folders appropriately.

#### Banner Images ####

The splash page has a simple banner carousel. While these image paths are hard coded into the splash page, the images are stored together for convenience in seedstore/images/splashbanners/banner*.*.

#### Missing Image Image ####

There is an image used when a product's image is missing: seedstore/images/missing.jpg. While the image is not pretty, it is used when the store cannot find any image for a particular product. This is a step up from showing the standard html missing image icon.

## The following AJAX section is deprecated ##

The back-end folder has been folded into the front-end, meaning there is one repository each per project. Because of this change in structure, paths do not need to be changed when creating a new store.

*********************************

### AJAX Calls ###

AJAX is a large part of the store functionality. Almost every time the user clicks something, an AJAX call is made. Since you are changing the folder names, each of these ajax calls needs to be changed to point to the appropriate backend. Checkout the [Polymer Ajax Documentaion](https://elements.polymer-project.org/elements/iron-ajax) All of the ajax calls are in the following locations:

* elements/pam-pages-container/pam-pages-container.html
* elements/pam-purchase/pam-purchase.html
* elements/pam-splash/pam-splash.html

To change a particular ajax call, take the origianl ajax element:

    <iron-ajax
        id="ajaxRootFetch"
        url="../../backend/index.php/site/rootFetch"
        handle-as="json"
        method="GET"
        on-response="ajaxRootFetchResponse"
        last-response="{{productRoot}}"
        on-error="ajaxRootFetchError">
    </iron-ajax>

and change the url attribute to point to your new backend:

    <iron-ajax
        id="ajaxRootFetch"
        url="../../../YourNewBackend/index.php/site/rootFetch"
        handle-as="json"
        method="GET"
        on-response="ajaxRootFetchResponse"
        last-response="{{productRoot}}"
        on-error="ajaxRootFetchError">
    </iron-ajax>

The paths are not case sensitive. Do this for each ajax call and you should now have a working store.

**********************************

### Change the RECSOURCE in the back-end ###

Go to the main.php config file. Go to the parameters section. Change the recsource parameter to the recsource you want. Taa-daa.

### Checkout Interface Parameters ###

Print and mail uses one checkout and payment application for all the online stores. You need to set a couple parameters so that the checkout behaves properly in connection with your store.

In the file elements/pam-pages-container/pam-pages-container.js, there is a function called setCheckoutParameters. There are currently four parameters:

* "theme": a key word for checkout CSS to know which CSS to use. You may create a CSS for the checkout to show a style for it otherwise you may use 'default' as the value for the default css.
* "vendor": same as "theme", used for compatibility
* "returnURL": the url for the checkout to go back to the store
* "languageCode": if you enable multi-language support, the checkout can keep the user's language preference (there are several languages currently supported)

Each of these values is set in sessionStorage and the checkout application knows how to use them.

### Update the Splash Page ###

The splash page is a fully customizable page, while the rest of the store is mostly static (in terms of customization). For example, Seed Store loads the child products of the root node, but it also contains another "product" which is a glorified link to another store: Youth Event Journals. Currently the splash page is displayed between the header and footer, but the header and footer can be hidden if you do not want them to appear on the splash page.

#### Hiding the Header and Footer on the Splash Page ####

TODO

### Update the CSS ###

Unless you want your store to look just like the Seed Store (this is discouraged because it might confuse some customers), you will need to change the style of the store. Polymer is a framework which supports the web components paradigm. This means many of the features, and styles, of the application are encapsulated. The folder seedstore/elements/ contains each of the custom-made components of the store.

Within the folders of each component is a style sheet of the form *custom-element-*styles.html (Polymer deprecated the use of .css type style sheets. Before you get all worried, Polymer just wraps css in an .html file. See [this](https://www.polymer-project.org/1.0/docs/devguide/styling) for a more comprehensive guide.) Since styles are scoped (and do not cascade down or leak up), there is also a *shared-styles* style sheet to help with theming.

Changing the style will likely be the most difficult part of making the store because variable names are... variable. It is convenient, though, that the style for each element can be found in either its own style sheet or the shared one. Note that *flex* is used all over the place. Flex is very useful for organizing and centering, so if you are not familiar with it, [learn it](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) and it will save you time and frustration.

### Create CSS for the checkout ###

The checkout dynamically loads different style sheets based on the session storage keys "theme" or "vendor". You can create a new theme to correspond to your store so that the checkout looks and feels more like your store than a separate app.

Good luck!


## How is the Store Structured? ##

TODO